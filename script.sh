#!/bin/bash

# install latest node
curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -

sudo apt-get update

sudo apt-get install -y nodejs

# install angular CLI
sudo npm install -g @angular/cli

# update angular
npm install

cd /var/www/html/sb/api

php index.php tools migrate

echo " ### NOTICE ######   Please ensure /etc/apache2/sites-enabled/000-default.conf is correctly setup for Document Root"

