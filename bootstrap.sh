#!/usr/bin/env bash

apt-get update
apt-get install -y apache2
echo "APACHE VERSION" >> system.log
apache2 -v >> system.log
apt-get install -y php7.0 php7.0-common php7.0-mbstring php7.0-xml php7.0-curl php7.0-cli
php -v >> system.log
apt-get install mariadb-server mariadb-client
apt-get install php7.0-mysql
if ! { -L /var/www ]; then
	rm -rf /var/www
	ln -fs /vagrant /var/www
fi
