## README

#### Setup Vagrant box for ease of standardize environment for development

----

**Packages includes:**

- Ubuntu 16.04.5 LTS Xenial64
- PHP 7.0.32
- Apache 2.4.18 (Ubuntu)
- Nodejs v4.2.6
- npm v3.5.2
- Composer v1.0.0-beta2
- Mysql (Mariadb) v10.0.36

**Prerequisite**

- Git & Git Bash (if windows)
- [Vagrant](https://www.vagrantup.com/downloads.html) installed
- [Virtual Box](https://www.virtualbox.org/wiki/Downloads) installed

**Guide**

1. Launch Git Bash (or any terminal that can run vagrant command)
2. To verify, run `vagrant -v` if is working in bash
3. cd to _`/your/work/path`_
4. issue command `git clone git@bitbucket.org:byronwongsvo/vagrant_sb.git your_project --recursive` to ensure able to get the sb files
5. cd to `your_project`
6. issue `vagrant box add byronwong/vagrantsb`
7. After download has completed, you may run `vagrant up` to bring up the environment
8. If you want to access the content and enter the environment, issue the command `vagrant ssh`
9. Once you are done, you may issue `vagrant suspend` , `vagrant halt` or `vagrant destroy`
_[not sure which command?](https://www.vagrantup.com/intro/getting-started/teardown.html)_
10. Things to take note on is that the submodule, which is inside the `src/sb` will checkout the commit point of the master branch itself, you can `cd src/sb` and `git checkout master` to ensure the work is at the master

---

References

[Vagrant Getting Started Guide](https://www.vagrantup.com/intro/getting-started/index.html)

Below configurations are based on the Vagrantfile in the repo

|     | User   | Password | IP Address | Port | Database |
|----:|:--------:|:------:|-----------:|:----:|---------:|
| Mariadb   | vagrant| vagrant | 168.68.33.10 | 3306 | sbdb|
| Static IP Web  |    |  |127.68.33.10|||
| Local Host Web |    |  |127.0.0.1|8080||